import { TestBed } from '@angular/core/testing';

import { IsNavbarHideService } from './is-navbar-hide.service';

describe('IsNavbarHideService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IsNavbarHideService = TestBed.get(IsNavbarHideService);
    expect(service).toBeTruthy();
  });
});
