import { Component, OnInit } from '@angular/core';
import { IsNavbarHideService } from '../service/is-navbar-hide.service';

@Component({
  selector: 'app-accueil-navbar',
  templateUrl: './accueil-navbar.component.html',
  styleUrls: ['./accueil-navbar.component.scss']
})
export class AccueilNavbarComponent implements OnInit {
  isNavbarHide: boolean;
  constructor(public variableinservice: IsNavbarHideService) {


  }

  ngOnInit() {
  }

}
