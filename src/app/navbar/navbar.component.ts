import { Component, OnInit } from '@angular/core';
import { IsNavbarHideService } from '../service/is-navbar-hide.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  isNavbarHide: boolean;

  constructor(public variableinservice: IsNavbarHideService) {
  }

  ngOnInit() {
  }

}
