import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IsNavbarHideService } from './service/is-navbar-hide.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { MenuleftComponent } from './menuleft/menuleft.component';
import { BodyComponent } from './body/body.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { ConnexionComponent } from './connexion/connexion.component';
import { LogoComponent } from './logo/logo.component';
import { PageConnexionComponent } from './page-connexion/page-connexion.component';
import { PageInscriptionComponent } from './page-inscription/page-inscription.component';
import { PageLogoComponent } from './page-logo/page-logo.component';
import { AccueilComponent } from './accueil/accueil.component';
import { AccueilNavbarComponent } from './accueil-navbar/accueil-navbar.component';
import { AccueilMenuleftComponent } from './accueil-menuleft/accueil-menuleft.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
const appRoutes: Routes = [
  { path: 'connexion', component: LogoComponent },
  { path: 'vue', component: NavbarComponent },
  { path: 'acceuil/vue', component: NavbarComponent },
  { path: 'vue/vue', component: NavbarComponent },
  { path: 'acceuil', component: AccueilNavbarComponent },
  { path: 'inscription', component:  PageLogoComponent },
  { path: 'connexion/inscription', component:  PageLogoComponent },
  { path: '', component: LogoComponent},
  { path: '', redirectTo: 'connexion', pathMatch: 'full' },
  { path: '**', redirectTo: 'connexion' }

];
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    MenuleftComponent,
    BodyComponent,
    InscriptionComponent,
    ConnexionComponent,
    LogoComponent,
    PageConnexionComponent,
    PageInscriptionComponent,
    PageLogoComponent,
    AccueilComponent,
    AccueilNavbarComponent,
    AccueilMenuleftComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes) ,
    AppRoutingModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [IsNavbarHideService],
  bootstrap: [AppComponent]
})
export class AppModule { }
