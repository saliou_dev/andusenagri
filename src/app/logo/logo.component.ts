import { Component, OnInit } from '@angular/core';
import { IsNavbarHideService } from '../service/is-navbar-hide.service';

@Component({
  selector: 'app-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.scss']
})
export class LogoComponent implements OnInit {
  isNavbarHide: boolean;

  constructor(public variableinservice: IsNavbarHideService) {
  }

  ngOnInit() {
    this.variableinservice.isNavbarHide = false;

  }

}
