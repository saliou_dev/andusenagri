import { Component, OnInit } from '@angular/core';
import { IsNavbarHideService } from '../service/is-navbar-hide.service';

@Component({
  selector: 'app-page-logo',
  templateUrl: './page-logo.component.html',
  styleUrls: ['./page-logo.component.scss']
})
export class PageLogoComponent implements OnInit {
   isNavbarHide: boolean;

  constructor( public variableinservice: IsNavbarHideService) {


   }

  ngOnInit() {
    this.variableinservice.isNavbarHide = false;
  }



}
