import { Component , OnInit} from '@angular/core';
import { IsNavbarHideService } from './service/is-navbar-hide.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'my-app';
  isNavbarHide: boolean;
  constructor(public variableinservice: IsNavbarHideService) {


  }

  ngOnInit() {
  }
}
