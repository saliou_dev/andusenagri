import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccueilMenuleftComponent } from './accueil-menuleft.component';

describe('AccueilMenuleftComponent', () => {
  let component: AccueilMenuleftComponent;
  let fixture: ComponentFixture<AccueilMenuleftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccueilMenuleftComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccueilMenuleftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
